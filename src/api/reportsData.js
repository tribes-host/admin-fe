export const reports = [
  {
    id: '1',
    timestamp: '2019/4/12',
    local: true,
    from: 'John', // actor nickname
    object: 'Bob', // user nickname
    header: 'Report #1', // content
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Nick', text: 'Lorem ipsum', id: '1', timestamp: '2019/4/13' },
      { author: 'Val', text: 'dolor sit amet', id: '2', timestamp: '2019/4/13' }
    ]
  },
  {
    id: '2',
    timestamp: '2019/4/1',
    local: true,
    from: 'Max',
    object: 'Vic',
    header: 'Report #2',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Tony', text: 'consectetur adipiscing elit', id: '3', timestamp: '2019/4/2' },
      { author: 'Zac', text: 'sed do eiusmod tempor incididunt', id: '4', timestamp: '2019/4/3' }
    ]
  },
  {
    id: '3',
    timestamp: '2019/2/28',
    local: true,
    from: 'Tim',
    object: 'Jen',
    header: 'Report #3',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [{ author: 'Bruce', text: 'ut labore et dolore magna aliqua', id: '5', timestamp: '2019/3/1' }]
  },
  {
    id: '4',
    timestamp: '2019/4/12',
    local: true,
    from: 'John', // actor nickname
    object: 'Bob', // user nickname
    header: 'Report #4', // content
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Nick', text: 'Lorem ipsum', id: '6', timestamp: '2019/4/13' },
      { author: 'Val', text: 'dolor sit amet', id: '7', timestamp: '2019/4/13' }
    ]
  },
  {
    id: '5',
    timestamp: '2019/4/1',
    local: true,
    from: 'Max',
    object: 'Vic',
    header: 'Report #5',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Tony', text: 'consectetur adipiscing elit', id: '8', timestamp: '2019/4/2' },
      { author: 'Zac', text: 'sed do eiusmod tempor incididunt', id: '9', timestamp: '2019/4/3' }
    ]
  },
  {
    id: '6',
    timestamp: '2019/2/28',
    local: true,
    from: 'Tim',
    object: 'Jen',
    header: 'Report #6',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [{ author: 'Bruce', text: 'ut labore et dolore magna aliqua', id: '10', timestamp: '2019/3/1' }]
  },
  {
    id: '7',
    timestamp: '2019/4/12',
    local: true,
    from: 'John', // actor nickname
    object: 'Bob', // user nickname
    header: 'Report #7', // content
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Nick', text: 'Lorem ipsum', id: '11', timestamp: '2019/4/13' },
      { author: 'Val', text: 'dolor sit amet', id: '12', timestamp: '2019/4/13' }
    ]
  },
  {
    id: '8',
    timestamp: '2019/4/1',
    local: true,
    from: 'Max',
    object: 'Vic',
    header: 'Report #8',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Tony', text: 'consectetur adipiscing elit', id: '13', timestamp: '2019/4/2' },
      { author: 'Zac', text: 'sed do eiusmod tempor incididunt', id: '14', timestamp: '2019/4/3' }
    ]
  },
  {
    id: '9',
    timestamp: '2019/2/28',
    local: true,
    from: 'Tim',
    object: 'Jen',
    header: 'Report #9',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [{ author: 'Bruce', text: 'ut labore et dolore magna aliqua', id: '15', timestamp: '2019/3/1' }]
  },
  {
    id: '10',
    timestamp: '2019/4/12',
    local: true,
    from: 'John', // actor nickname
    object: 'Bob', // user nickname
    header: 'Report #10', // content
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Nick', text: 'Lorem ipsum', id: '16', timestamp: '2019/4/13' },
      { author: 'Val', text: 'dolor sit amet', id: '17', timestamp: '2019/4/13' }
    ]
  },
  {
    id: '11',
    timestamp: '2019/4/1',
    local: true,
    from: 'Max',
    object: 'Vic',
    header: 'Report #11',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Tony', text: 'consectetur adipiscing elit', id: '18', timestamp: '2019/4/2' },
      { author: 'Zac', text: 'sed do eiusmod tempor incididunt', id: '19', timestamp: '2019/4/3' }
    ]
  },
  {
    id: '12',
    timestamp: '2019/2/28',
    local: true,
    from: 'Tim',
    object: 'Jen',
    header: 'Report #12',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [{ author: 'Bruce', text: 'ut labore et dolore magna aliqua', id: '20', timestamp: '2019/3/1' }]
  },
  {
    id: '13',
    timestamp: '2019/4/12',
    local: true,
    from: 'John', // actor nickname
    object: 'Bob', // user nickname
    header: 'Report #13', // content
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Nick', text: 'Lorem ipsum', id: '21', timestamp: '2019/4/13' },
      { author: 'Val', text: 'dolor sit amet', id: '22', timestamp: '2019/4/13' }
    ]
  },
  {
    id: '14',
    timestamp: '2019/4/1',
    local: true,
    from: 'Max',
    object: 'Vic',
    header: 'Report #14',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Tony', text: 'consectetur adipiscing elit', id: '23', timestamp: '2019/4/2' },
      { author: 'Zac', text: 'sed do eiusmod tempor incididunt', id: '24', timestamp: '2019/4/3' }
    ]
  },
  {
    id: '15',
    timestamp: '2019/2/28',
    local: true,
    from: 'Tim',
    object: 'Jen',
    header: 'Report #15',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [{ author: 'Bruce', text: 'ut labore et dolore magna aliqua', id: '25', timestamp: '2019/3/1' }]
  },
  {
    id: '16',
    timestamp: '2019/4/12',
    local: true,
    from: 'John', // actor nickname
    object: 'Bob', // user nickname
    header: 'Report #16', // content
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Nick', text: 'Lorem ipsum', id: '26', timestamp: '2019/4/13' },
      { author: 'Val', text: 'dolor sit amet', id: '27', timestamp: '2019/4/13' }
    ]
  },
  {
    id: '17',
    timestamp: '2019/4/1',
    local: true,
    from: 'Max',
    object: 'Vic',
    header: 'Report #17',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [
      { author: 'Tony', text: 'consectetur adipiscing elit', id: '28', timestamp: '2019/4/2' },
      { author: 'Zac', text: 'sed do eiusmod tempor incididunt', id: '29', timestamp: '2019/4/3' }
    ]
  },
  {
    id: '18',
    timestamp: '2019/2/28',
    local: true,
    from: 'Tim',
    object: 'Jen',
    header: 'Report #18',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    notes: [{ author: 'Bruce', text: 'ut labore et dolore magna aliqua', id: '30', timestamp: '2019/3/1' }]
  }
]
